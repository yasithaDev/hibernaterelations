package com.oknow.springmvchibernate.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.oknow.springmvchibernate.Entities.Customer;
import com.oknow.springmvchibernate.Entities.Order;
import com.oknow.springmvchibernate.Entities.Pet;
import com.oknow.springmvchibernate.Entities.Student;
import com.oknow.springmvchibernate.Entities.Subject;
import com.oknow.springmvchibernate.daos.PetDAO;

@RestController
@RequestMapping("/")
public class HelloController 
{
	@Autowired
	private PetDAO petDAO;
	
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String sayHello(ModelMap model) 
	{
		Pet pet = new Pet();
		pet.setName("AAsdf");
		petDAO.save(pet);
		System.out.println("Pet::"+pet);
		return "success";
	}
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	public ArrayList<Pet> select(ModelMap model) 
	{
		ArrayList<Pet> petList=petDAO.select();
		//System.out.println("Pet::"+pet);
		return petList;
	}
	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	public ArrayList<Customer> customer(ModelMap model) 
	{
		ArrayList<Customer> customers = petDAO.customer();
		//System.out.println("Pet::"+pet);
		return customers;
	}
	
	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	public ArrayList<Order> order(ModelMap model) 
	{
		ArrayList<Order> orders = petDAO.orders();
		//System.out.println("Pet::"+pet);
		return orders;
	}
	@RequestMapping(value = "/student", method = RequestMethod.GET)
	public ArrayList<Student> student(ModelMap model) 
	{
		ArrayList<Student> students = petDAO.student();
		//System.out.println("Pet::"+pet);
		return students;
	}
	
	@RequestMapping(value = "/subject", method = RequestMethod.GET)
	public ArrayList<Subject> subject(ModelMap model) 
	{
		ArrayList<Subject> subjects = petDAO.subject();
		//System.out.println("Pet::"+pet);
		return subjects;
	}
}
