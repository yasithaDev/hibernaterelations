package com.oknow.springmvchibernate.Entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "student")
public class Student 
{
	@Id
	@Column(name = "stdnt_id")
	int stdnt_id;
	
	@Column(name = "name")
	String name;
	
	@Column(name = "gread")
	int gread;
	
    @ManyToMany(cascade = { CascadeType.ALL },fetch=FetchType.EAGER)
    @JoinTable(
        name = "student_subject", 
        joinColumns = { @JoinColumn(name = "stdnt_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "subject_id") }
    )
    Set<Subject> subjects = new HashSet<>();
	
	public Set<Subject> getSubjects() 
	{

		return subjects;
	}
	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}
	public int getStdnt_id() {
		return stdnt_id;
	}
	public void setStdnt_id(int stdnt_id) {
		this.stdnt_id = stdnt_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getGread() {
		return gread;
	}
	public void setGread(int gread) {
		this.gread = gread;
	}
}
