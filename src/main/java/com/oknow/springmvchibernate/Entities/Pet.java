package com.oknow.springmvchibernate.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pet")
public class Pet 
{
	@Id
	@Column(name = "name")
	String name;
	
	@Column(name = "owner")
	String owner;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String onwer) {
		this.owner = onwer;
	}
}