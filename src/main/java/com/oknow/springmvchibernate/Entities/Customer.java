package com.oknow.springmvchibernate.Entities;

import java.util.ArrayList;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
 
@Entity
@Table(name = "customer")
public class Customer 
{
	@Id
	@Column(name = "cus_id")
	int cus_id;
	
	@Column(name="name")
	String name;
	
	@Column(name="address")
	String address;
	
	@OneToMany(mappedBy="customer",fetch=FetchType.EAGER)
	Set<Order> orders;
	
	public Set<Order> getOrders() {
		return orders;
	}
	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}
	public int getCus_id() {
		return cus_id;
	}
	public void setCus_id(int cus_id) {
		this.cus_id = cus_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
