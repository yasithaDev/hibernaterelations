package com.oknow.springmvchibernate.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "orders")
public class Order 
{
	@Id	
	@Column(name="order_id")
	int order_id;
	/*
	@Column(name="cus_id")
	String cus_id; 


	public String getCus_id() {
		return cus_id;
	}
	public void setCus_id(String cus_id) {
		this.cus_id = cus_id;
	}
	 */
	
	@Column(name="address")
	String address;
	
	@Column(name="gross_amount")
	String gross_amount;
	
	//@JsonIgnore 
	@ManyToOne
	@JoinColumn(name="cus_id",nullable=true)
    private Customer customer;
	
	public Customer getCustomer() {
		customer.setOrders(null);
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public int getOrder_id() {
		return order_id;
	}
	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}
	

	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getGross_amount() {
		return gross_amount;
	}
	public void setGross_amount(String gross_amount) {
		this.gross_amount = gross_amount;
	}

}

