package com.oknow.springmvchibernate.Entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Table(name = "subject")
public class Subject 
{
	@Id
	@Column(name = "subject_id")
	int subject_id;
	
	@Column(name = "subject_name")
	String subject_name;
	
    @ManyToMany(mappedBy = "subjects",fetch=FetchType.EAGER)
    private Set<Student> students = new HashSet<>();
	
	public Set<Student> getStudents() {
		for (Student s : students) {
		    s.setSubjects(null);
		}
		return students;
	}
	public void setStudents(Set<Student> students) {
		this.students = students;
	}
	public int getSubject_id() {
		return subject_id;
	}
	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}
	public String getSubject_name() {
		return subject_name;
	}
	public void setSubject_name(String subject_name) {
		this.subject_name = subject_name;
	}
}
