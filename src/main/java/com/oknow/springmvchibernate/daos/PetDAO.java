package com.oknow.springmvchibernate.daos;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.oknow.springmvchibernate.Entities.Customer;
import com.oknow.springmvchibernate.Entities.Order;
import com.oknow.springmvchibernate.Entities.Pet;
import com.oknow.springmvchibernate.Entities.Student;
import com.oknow.springmvchibernate.Entities.Subject;

@Component
public class PetDAO 
{
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void save(Pet p) 
	{
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(p);
		tx.commit();

	}
	public ArrayList<Pet> select() 
	{
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(Pet.class);
		ArrayList<Pet> petList = (ArrayList<Pet>) criteria.list();
		tx.commit();
		return petList;
	}
	public ArrayList<Customer> customer() 
	{
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(Customer.class);
		ArrayList<Customer> customers = (ArrayList<Customer>) criteria.list();
		tx.commit();
		return customers;
	}
	public ArrayList<Order> orders() 
	{
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(Order.class);
		ArrayList<Order> orders = (ArrayList<Order>) criteria.list();
		tx.commit();
		return orders;
	}
	
	
	public ArrayList<Student> student() 
	{
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(Student.class);
		ArrayList<Student> students = (ArrayList<Student>) criteria.list();
		tx.commit();
		return students;
	}
	public ArrayList<Subject> subject() 
	{
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(Subject.class);
		ArrayList<Subject> subjects = (ArrayList<Subject>) criteria.list();
		tx.commit();
		return subjects;
	}
	
}

